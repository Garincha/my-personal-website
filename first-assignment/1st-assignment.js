let userDetails = {
    squadName: "Super hero squad",
    homeTown: "Metro City",
    formed: '2016',
    secretBase: "Super tower",
    active: true,
    members: [
      {
        name: "Molecule Man",
        age: 29,
        secretIdentity: "Dan Jukes",
        powers: ["Radiation resistance", "Turning tiny", "Radiation blast"]
      },
      {
        name: "Madame Uppercut",
        age: 39,
        secretIdentity: "Jane Wilson",
        powers: [
          "Million tonne punch",
          "Damage resistance",
          "Superhuman reflexes"
        ]
      },
      {
        name: "Eternal Flame",
        age: 1000000,
        secretIdentity: "Unknown",
        powers: [
          "Immortality",
          "Heat Immunity",
          "Inferno",
          "Teleportation",
          "Interdimensional travel"
        ]
      }
    ]
  };
  
  function getData(){
      let container = document.getElementById('root')
      let h1 = document.createElement('h1');
      let h1Txt = document.createTextNode(userDetails.squadName)
      h1.appendChild(h1Txt)
  
       let span1 = document.createElement('span');
       let txt3 = document.createTextNode('homeTown,');
       span1.appendChild(txt3)
  
  
       let span2 = document.createElement('span');
       let txt1 = document.createTextNode(userDetails.homeTown)
       span2.appendChild(txt1)
  
       let address = document.createElement('address');
       address.appendChild(span1)
       address.appendChild(span2)
  
       let bold = document.createElement('b');
       let boldTxt1 = document.createTextNode('Formed :')
       let boldTxt2 = document.createTextNode(userDetails.formed)
       bold.appendChild(boldTxt1)
       bold.appendChild(boldTxt2)
     
       container.appendChild(h1)
       container.appendChild(address)
       container.appendChild(bold)
  
  }
  
  
  
  
  
  
  
  
  
  const John = {};
  John.HTMLTable = {
    buildTH: function(tableHeadData) {
      let txt = document.createTextNode(tableHeadData);
      let th = document.createElement("th");
      th.appendChild(txt);
      return th;
    },
    buildTableHeadRow: function(tRawData) {
      let tr = document.createElement("tr");
      for (let property in tRawData) {
        tr.appendChild(this.buildTH(tRawData[property]));
      }
  
      return tr;
    },
  
    buildTBody: function(collection) {
      let txt = document.createTextNode(collection);
      let td = document.createElement("td");
      td.appendChild(txt);
      return td;
    },
    createUl2: function() {
      let td = document.createElement("td");
      let ul = document.createElement("ul");
      let li = null;
      for (let userDetail of userDetails.members) {
        for (let data in userDetail.powers) {
          li = document.createElement("li");
          let txt = document.createTextNode(userDetail.powers[data]);
          li.appendChild(txt);
        }
        ul.appendChild(li);
      }
  
      td.appendChild(ul);
      return td;
    },
    buildTableBody: function(collections) {
      let tbody = document.createElement("tbody");
  
      for (let data of collections) {
        let tr = document.createElement("tr");
        tr.appendChild(this.buildTBody(data.name));
        tr.appendChild(this.buildTBody(data.age));
        tr.appendChild(this.buildTBody(data.secretIdentity));
  
        tr.appendChild(this.createUl2());
        tbody.appendChild(tr);
      }
      return tbody;
    },
  
    buildTable: function(collection, container) {
      getData();
      let table = document.createElement("table");
      let titles = Object.keys(collection.members[0]);
      let thead = this.buildTableHeadRow(titles);
      let tbody = this.buildTableBody(collection.members);
      table.appendChild(thead);
      table.appendChild(tbody);
      table.setAttribute("border", "1");
  
      let domContainer = document.getElementById(container);
      domContainer.appendChild(table);
     
    }
  };
  
  
  